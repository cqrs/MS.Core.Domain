﻿using System;
using System.Globalization;

namespace MS.Core
{
    public static class StringExtensionMethods
	{
		public static Guid ToGuid(this string value)
		{
			Guid guid;
		    return !Guid.TryParse(value, out guid) ? Guid.Empty : guid;
		}

        public static string FormatWith(this string format, params object[] args)
        {
            return string.Format(CultureInfo.InvariantCulture, format ?? string.Empty, args);
        }

	}
}