﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace MS.Core.Reflection
{
    public static class ExpressionExtensionMethods
    {
        /// <summary>
        /// Converts an expression into a <see cref="MemberInfo"/>.
        /// </summary>
        /// <param name="expression">The expression to convert.</param>
        /// <returns>The member info.</returns>
        private static MemberInfo GetMemberInfo(this Expression expression)
        {
            var lambda = (LambdaExpression)expression;

            MemberExpression memberExpression;
            if (lambda.Body is UnaryExpression)
            {
                var unaryExpression = (UnaryExpression)lambda.Body;
                memberExpression = (MemberExpression)unaryExpression.Operand;
            }
            else
                memberExpression = (MemberExpression)lambda.Body;

            return memberExpression.Member;
        }


        /// <summary>
        /// Returns the member name of a specified object.
        /// </summary>
        /// <typeparam name="TMember">The type of the member.</typeparam>
        /// <param name="o">The specified object.</param>
        /// <param name="member">The member.</param>
        /// <returns>The member name.</returns>
        public static string NameOfMember<TMember>(this object o, Expression<Func<TMember>> member)
        {
            return GetMemberInfo(member).Name;
        }
    }
}
