﻿using System;

namespace MS.Core.Domain
{
    public interface IEntity
    {
        Guid Id { get; }
        void ApplyEvent(object @event);
        IAggregate Parent();
    }
}