﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MS.Core.Domain
{
    public interface IPublish : IDisposable
    {
        Task Publish<T>(T message, CancellationToken cancellationToken = new CancellationToken()) where T : class;
        Task Publish(object message, CancellationToken cancellationToken = new CancellationToken());
        Task Publish(object message, Type messageType, CancellationToken cancellationToken = new CancellationToken());
        Task Publish<T>(object values, CancellationToken cancellationToken = new CancellationToken()) where T : class;
    }
}
