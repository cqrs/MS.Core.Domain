﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MS.Core.Domain
{
    public interface IAggregate
    {
        Guid Id { get; }
        long Version { get; }

        /// <summary>
        /// Events that have been applied and await persistance
        /// </summary>
        IEnumerable<object> UncommittedEvents { get; }

        void ClearAppliedEvents();

        /// <summary>
        /// Applies an event to the instance augmenting the current version
        /// for each event applied
        /// </summary>
        /// <param name="event">Event to apply</param>
        /// <param name="isNew">True if the event is new to the event stream; otherwise false</param>
        Task ApplyEvent(object @event, bool isNew = true);

        /// <summary>
        /// Replays a stream of events to bring the instance up to the current
        /// state and version
        /// </summary>
        /// <param name="events">Stream of events to replay</param>
        void ReplayEvents(IEnumerable<object> events);

        IMemento GetSnapshot();
    }
}
