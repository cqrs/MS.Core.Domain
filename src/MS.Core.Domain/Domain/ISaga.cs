using System.Collections.Generic;

namespace MS.Core.Domain
{
    public interface ISaga
	{
		string Id { get; }

		int Version { get; }

		void Transition(object message);

		IEnumerable<object> UncommittedEvents { get; }

		void ClearUncommittedEvents();

        IEnumerable<object> UndispatchedMessages { get; }

		void ClearUndispatchedMessages();
	}
}