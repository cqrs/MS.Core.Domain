﻿using System;

namespace MS.Core.Domain.Persistence
{
    public interface IReadonlySagaRepository : IDisposable
    {
        TSaga GetById<TSaga>(string bucketId, string sagaId) where TSaga : class, ISaga;
    }
}