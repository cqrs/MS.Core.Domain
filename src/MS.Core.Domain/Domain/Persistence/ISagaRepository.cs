﻿using System;
using System.Collections.Generic;

namespace MS.Core.Domain.Persistence
{
    public interface ISagaRepository : IReadonlySagaRepository
    {
        void Save(string bucketId, ISaga saga, Guid commitId, Action<IDictionary<string, object>> updateHeaders, Action<IEnumerable<object>> dispatch);
    }
}