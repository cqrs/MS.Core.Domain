﻿using System;

namespace MS.Core.Domain.Persistence
{
    public interface IConstructSagas
    {
        ISaga Build(Type type, string id);
    }
}