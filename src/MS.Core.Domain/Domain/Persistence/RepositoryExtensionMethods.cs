﻿using System;
using System.Collections.Generic;

namespace MS.Core.Domain.Persistence
{
    public static class RepositoryExtensionMethods
    {
        public static void Save(this IRepository repository, IAggregate aggregate, Guid commitId, Action<IEnumerable<object>> dispatch)
        {
            repository.Save(aggregate, commitId, a => { }, dispatch);
        }

        public static void Save(this IRepository repository, string bucketId, IAggregate aggregate, Guid commitId, Action<IEnumerable<object>> dispatch)
        {
            repository.Save(bucketId, aggregate, commitId, a => { }, dispatch);
        }
    }
}
