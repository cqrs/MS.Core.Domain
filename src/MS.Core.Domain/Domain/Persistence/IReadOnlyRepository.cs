﻿using System;

namespace MS.Core.Domain.Persistence
{
    public interface IReadOnlyRepository : IDisposable
    {
        TAggregate GetById<TAggregate>(Guid id) where TAggregate : class, IAggregate;
        TAggregate GetById<TAggregate>(Guid id, int version) where TAggregate : class, IAggregate;
        TAggregate GetById<TAggregate>(string bucketId, Guid id) where TAggregate : class, IAggregate;
        TAggregate GetById<TAggregate>(string bucketId, Guid id, int version) where TAggregate : class, IAggregate;
    }
}