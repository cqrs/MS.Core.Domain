﻿using System;

namespace MS.Core.Domain.Persistence
{
    public interface IConstructAggregates
    {
        IAggregate Build(Type type, Guid id, IMemento snapshot);
    }
}
