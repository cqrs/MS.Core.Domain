﻿using System;
using System.Collections.Generic;

namespace MS.Core.Domain.Persistence
{
    public interface IRepository : IReadOnlyRepository
    {
        void Save(IAggregate aggregate, Guid commitId, Action<IDictionary<string, object>> updateHeaders, Action<IEnumerable<object>> dispatch);

        void Save(string bucketId, IAggregate aggregate, Guid commitId, Action<IDictionary<string, object>> updateHeaders, Action<IEnumerable<object>> dispatch);
    }
}