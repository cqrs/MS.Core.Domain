﻿using System;
using System.Collections.Generic;

namespace MS.Core.Domain.Persistence
{
    public static class SagaRepositoryExtensionMethods
    {
        public static TSaga GetById<TSaga>(this ISagaRepository sagaRepository, Guid sagaId)
            where TSaga : class, ISaga
        {
            return sagaRepository.GetById<TSaga>(BucketId.Default, sagaId.ToString());
        }

        public static void Save(
            this ISagaRepository sagaRepository,
            ISaga saga,
            Guid commitId,
            Action<IDictionary<string, object>> updateHeaders,
            Action<IEnumerable<object>> dispatch)
        {
            sagaRepository.Save(BucketId.Default, saga, commitId, updateHeaders, dispatch);
        }

        public static TSaga GetById<TSaga>(this ISagaRepository sagaRepository, string sagaId)
            where TSaga : class, ISaga
        {
            return sagaRepository.GetById<TSaga>(BucketId.Default, sagaId);
        }

    }
}
