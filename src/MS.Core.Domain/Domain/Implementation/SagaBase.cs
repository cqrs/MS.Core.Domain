﻿using System;
using System.Collections.Generic;

namespace MS.Core.Domain.Implementation
{
    public abstract class SagaBase<TMessage> : ISaga, IEquatable<ISaga>
        where TMessage : class
    {
        private readonly IDictionary<Type, Action<TMessage>> _handlers = new Dictionary<Type, Action<TMessage>>();

        private readonly ICollection<TMessage> _uncommittedEvents = new List<TMessage>();

        private readonly ICollection<TMessage> _undispatchedMessages = new List<TMessage>();

        public virtual bool Equals(ISaga other)
        {
            return null != other && other.Id == Id;
        }

        public string Id { get; protected set; }

        public int Version { get; private set; }

        public void Transition(object message)
        {
            _handlers[message.GetType()](message as TMessage);
            _uncommittedEvents.Add(message as TMessage);
            Version++;
        }

        public IEnumerable<object> UncommittedEvents { get { return _uncommittedEvents; } }

        void ISaga.ClearUncommittedEvents()
        {
            _uncommittedEvents.Clear();
        }

        public IEnumerable<object> UndispatchedMessages { get { return _undispatchedMessages; } }

        void ISaga.ClearUndispatchedMessages()
        {
            _undispatchedMessages.Clear();
        }

        protected void Register<TRegisteredMessage>(Action<TRegisteredMessage> handler)
            where TRegisteredMessage : class, TMessage
        {
            _handlers[typeof (TRegisteredMessage)] = message => handler(message as TRegisteredMessage);
        }

        protected void Dispatch(TMessage message)
        {
            _undispatchedMessages.Add(message);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as ISaga);
        }
    }
}