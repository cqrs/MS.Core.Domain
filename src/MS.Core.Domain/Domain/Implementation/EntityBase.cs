﻿using System;

namespace MS.Core.Domain.Implementation
{
    /// <summary>
    /// Represents an object contained within the aggregate with its own identity
    /// and application of events
    /// </summary>
    public abstract class EntityBase : IEntity, IEquatable<IEntity>
    {
        private readonly AggregateBase _parent;

        public Guid Id { get; protected set; }

        protected EntityBase(AggregateBase parent, Guid entityId)
        {
            Id = entityId;
            _parent = parent;
            if (_parent != null) _parent.Associate(this);
        }

        public void ApplyEvent(object @event)
        {
            var domainEntityEvent = @event as IEntityEvent;
            if (domainEntityEvent != null)
            {
                domainEntityEvent.EntityId = Id;
            }
            _parent.ApplyEvent(@event);
        }

        public IAggregate Parent()
        {
            return _parent;
        }

        public virtual bool Equals(IEntity other)
        {
            return null != other && other.Id == Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as IEntity);
        }
    }
}
