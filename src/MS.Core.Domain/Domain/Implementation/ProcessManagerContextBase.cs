﻿using System;
using MS.Core.Domain.Persistence;

namespace MS.Core.Domain.Implementation
{
    /// <remarks>
    /// Shouldn't be used as singleton!
    /// </remarks>
    public abstract class ProcessManagerContextBase : IProcessManagerContext
    {
        private readonly ISagaRepository _sagas;
        private readonly ICommandPublish _commandBus;

        protected ProcessManagerContextBase(ISagaRepository sagas, ICommandPublish commandBus)
        {
            if (sagas == null) throw new ArgumentNullException("sagas");
            if (commandBus == null) throw new ArgumentNullException("commandBus");
            _sagas = sagas;
            _commandBus = commandBus;
        }

        public bool TryGetById<TSaga>(string bucketId, string sagaId, out TSaga saga) where TSaga : class, ISaga
        {
            saga = _sagas.GetById<TSaga>(bucketId, sagaId);
            return (saga != null);
        }

        public virtual void Finalize(string bucketId, ISaga saga, Guid commitId)
        {
            if (saga == null) throw new ArgumentNullException("saga");
            
            _sagas.Save(bucketId, saga, commitId, objects => {}, messages =>
            {
                foreach (var message in messages)
                {
                    _commandBus.Publish(message);
                }
            });
        }
    }
}
