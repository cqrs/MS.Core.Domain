﻿using System;
using System.Collections.Generic;
using MS.Core.Domain.Persistence;

namespace MS.Core.Domain.Implementation
{
    /// <remarks>
    /// Shouldn't be used as singleton!
    /// </remarks>
    public abstract class CommandHandlerContextBase : ICommandHandlerContext
    {
        private readonly IRepository _aggregates;
        private readonly IEventPublish _eventBus;

        protected CommandHandlerContextBase(IRepository aggregates, IEventPublish eventBus)
        {
            if (aggregates == null) throw new ArgumentNullException("aggregates");
            if (eventBus == null) throw new ArgumentNullException("eventBus");
            _aggregates = aggregates;
            _eventBus = eventBus;
        }

        public bool TryGetById<TAggregate>(Guid id, out TAggregate aggregate) where TAggregate : class, IAggregate
        {
            aggregate = _aggregates.GetById<TAggregate>(id);
            return (aggregate != null);
        }

        public bool TryGetById<TAggregate>(string bucketId, Guid id, out TAggregate aggregate) where TAggregate : class, IAggregate
        {
            aggregate = _aggregates.GetById<TAggregate>(id);
            return (aggregate != null);
        }

        public void Finalize(IAggregate aggregate, Guid commitId, bool broadcastOnly = false)
        {
            Finalize(BucketId.Default, aggregate, commitId, broadcastOnly);
        }

        public void Finalize(string bucketId, IAggregate aggregate, Guid commitId, bool broadcastOnly = false)
        {
            if (aggregate == null) throw new ArgumentNullException("aggregate");

            Action<IEnumerable<object>> dispatch = events =>
            {
                foreach (var @event in events)
                {
                    _eventBus.Publish(@event);
                }
            };
            
            if (!broadcastOnly)
            {
                _aggregates.Save(bucketId, aggregate, commitId, dispatch);
            }
            else
            {
                dispatch(aggregate.UncommittedEvents);
            }
        }
    }
}
