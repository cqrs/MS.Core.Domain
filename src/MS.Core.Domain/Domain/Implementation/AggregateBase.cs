using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using MS.Core.Reflection;

namespace MS.Core.Domain.Implementation
{
    public abstract class AggregateBase : IAggregate, IEquatable<IAggregate>
    {
        private readonly IList<object> _uncommittedEvents = new List<object>();
        private static readonly IDictionary<string, MethodInfo> CachedLocalMethods = new ConcurrentDictionary<string, MethodInfo>();
        private static readonly IDictionary<string, MethodInfo> CachedEntityMethods = new ConcurrentDictionary<string, MethodInfo>();
        private readonly List<IEntity> _entities = new List<IEntity>();

		public Guid Id { get; set; }

		public long Version { get; protected set; }

        /// <summary>
        /// Events that have been applied and await persistance
        /// </summary>
        public IEnumerable<object> UncommittedEvents { get { return _uncommittedEvents; } } 

        /// <summary>
        /// Creates a new aggregate with a new ID
        /// </summary>
        protected AggregateBase()
            : this(Guid.NewGuid())
        {
        }

        /// <summary>
        /// Creates a new aggregate
        /// </summary>
        /// <param name="id">ID of the aggregate</param>
        protected AggregateBase(Guid id)
        {
            Id = id;
        }

        /// <summary>
        /// Applies an event to the instance augmenting the current version
        /// for each event applied
        /// </summary>
        /// <param name="event">Event to apply</param>
        /// <param name="isNew">True if the event is new to the event stream; otherwise false</param>
        public async Task ApplyEvent(object @event, bool isNew = true)
        {
            if (@event == null) throw new ArgumentNullException("event");

            if ((@event is IEvent))
            {
                Version++;
                // Call the apply method on the domain model instance
                ApplyEventToSelf((IEvent)@event, isNew);

                if (@event is IEntityEvent)
                {
                    ApplyEventToEntities(@event as IEntityEvent, isNew);
                }
            }

            // Save the event for persistance if it's new
            if (isNew)
            {
                _uncommittedEvents.Add(@event);
            }
        }

        /// <summary>
        /// Replays a stream of events to bring the instance up to the current
        /// state and version
        /// </summary>
        /// <param name="events">Stream of events to replay</param>
        public void ReplayEvents(IEnumerable<object> events)
        {
            if (events == null)
            {
                return;
            }

            foreach (var domainEvent in events)
            {
                ApplyEvent(domainEvent, false);
            }
        }

        public void ClearAppliedEvents()
        {
            _uncommittedEvents.Clear();
        }

        /// <summary>
        /// Associates an entity with the aggregate root
        /// </summary>
        /// <param name="entity">Entity to associate</param>
        internal void Associate(IEntity entity)
        {
            if (!_entities.Contains(entity))
            {
                _entities.Add(entity);
            }
        }


        /// <summary>
        /// Applies a domain event to the current instance
        /// </summary>
        /// <param name="domainEvent">Event to apply</param>
        /// <param name="isNew">True if the event is new to the event stream; otherwise false</param>
        private void ApplyEventToSelf(IEvent domainEvent, bool isNew)
        {
            ApplyMethodWithCaching(this, domainEvent, isNew, CachedLocalMethods, false);
        }

        /// <summary>
        /// Applies an entity event to the appropriate entity
        /// </summary>
        /// <param name="domainEntityEvent">Entity event to apply</param>
        /// <param name="isNew">True if the event is new to the event stream; otherwise false</param>
        private void ApplyEventToEntities(IEntityEvent domainEntityEvent, bool isNew)
        {
            var entity = _entities.FirstOrDefault(e => e.Id == domainEntityEvent.EntityId);

            if (entity == null)
            {
                return;
            }

            ApplyMethodWithCaching(entity, domainEntityEvent, isNew, CachedEntityMethods, true);
        }

        private void ApplyMethodWithCaching(object instanceToApply, IEvent eventToApply, bool isNew, IDictionary<string, MethodInfo> cache, bool toEntity)
        {
            var eventType = eventToApply.GetType();

            var localKey = string.Format("{0},{1}", GetType().FullName, eventType);
            MethodInfo method;

            // Check of the handler (method info) for this event has been cached
            if (!cache.TryGetValue(localKey, out method))
            {
                // Get the convention-based handler
                if (!toEntity)
                {
                    var applyDomainEventType = typeof(IApplyEvent<>).MakeGenericType(eventType);
                    if (applyDomainEventType.IsInstanceOfType(instanceToApply))
                    {
                        method = applyDomainEventType.GetMethod("Apply");
                        cache.Add(localKey, method);
                    }
                }
                else if (eventToApply.GetType().IsSubclassOf(typeof(IEntityEvent)))
                {
                    var applyDomainEntityEventType = typeof(IApplyEntityEvent<>).MakeGenericType(eventType);
                    if (applyDomainEntityEventType.IsInstanceOfType(instanceToApply))
                    {
                        method = applyDomainEntityEventType.GetMethod("Apply");
                        cache.Add(localKey, method);
                    }
                }
            }

            if (method != null)
            {
                method.Invoke(instanceToApply, new object[] { eventToApply, isNew });
            }
        }

        /// <summary>
        /// Load state from a snapshot
        /// </summary>
        /// <param name="snapshot">Snapshot to load</param>
        protected abstract void LoadFromSnapshot<T>(T snapshot) where T : IMemento;

        IMemento IAggregate.GetSnapshot()
		{
			var snapshot = GetSnapshot();
		    if (snapshot == null) return null;
			snapshot.Id = Id;
			snapshot.Version = Version;
			return snapshot;
		}

        public void InitializeFromSnapshot(IMemento snapshot)
        {
            this.InvokeGenericMethod("LoadFromSnapshot", snapshot.GetType(), snapshot);
        }


        protected virtual IMemento GetSnapshot()
        {
            return null;
        }
        
        public virtual bool Equals(IAggregate other)
		{
			return null != other && other.Id == Id;
		}

		public override int GetHashCode()
		{
			return Id.GetHashCode();
		}

		public override bool Equals(object obj)
		{
			return Equals(obj as IAggregate);
		}
	}
}