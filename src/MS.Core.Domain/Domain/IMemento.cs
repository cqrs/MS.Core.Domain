using System;

namespace MS.Core.Domain
{
    public interface IMemento
	{
		Guid Id { get; set; }

		long Version { get; set; }
	}
}