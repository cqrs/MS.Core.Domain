﻿using System;

namespace MS.Core.Domain
{
    public interface IEntityEvent : IEvent
    {
        Guid EntityId { get; set; }
    }
}
