using System;

namespace MS.Core.Domain
{
    public interface IProcessManagerContext
    {
        bool TryGetById<TSaga>(string bucketId, string sagaId, out TSaga saga) where TSaga : class, ISaga;
        void Finalize(string bucketId, ISaga saga, Guid commitId);
    }
}