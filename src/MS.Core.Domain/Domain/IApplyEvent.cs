﻿namespace MS.Core.Domain
{
    public interface IApplyEvent<in TEvent> where TEvent : class, IEvent
    {
        void Apply(TEvent @event, bool isNew);
    }
}
