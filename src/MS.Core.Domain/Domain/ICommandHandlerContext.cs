﻿using System;

namespace MS.Core.Domain
{
    public interface ICommandHandlerContext
    {
        bool TryGetById<TAggregate>(Guid id, out TAggregate aggregate) where TAggregate : class, IAggregate;

        bool TryGetById<TAggregate>(string bucketId, Guid id, out TAggregate aggregate) where TAggregate : class, IAggregate;

        /// <summary>
        /// Persists event stream, publishes events, and creates snapshots
        /// </summary>
        /// <param name="aggregate">Aggregate root to persist</param>
        /// <param name="commitId">ID of the commit</param>
        /// <param name="broadcastOnly">Do not persist the evnet; only publish to event handlers.</param>
        void Finalize(IAggregate aggregate, Guid commitId, bool broadcastOnly = false);
        
        /// <summary>
        /// Persists event stream, publishes events, and creates snapshots
        /// </summary>
        /// <param name="bucketId">The bucket ID</param>
        /// <param name="aggregate">Aggregate root to persist</param>
        /// <param name="commitId">ID of the commit</param>
        /// <param name="broadcastOnly">Do not persist the evnet; only publish to event handlers.</param>
        void Finalize(string bucketId, IAggregate aggregate, Guid commitId, bool broadcastOnly = false);
    }
}
