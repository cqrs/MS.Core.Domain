﻿namespace MS.Core.Domain
{
    public interface IApplyEntityEvent<in TEntityEvent> where TEntityEvent : class, IEntityEvent
    {
        void Apply(TEntityEvent @event, bool isNew);
    }
}
