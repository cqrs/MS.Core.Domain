﻿using System;
using MassTransit;
using MassTransit.Util;
using MS.Core.Domain;
using MS.Core.Domain.Implementation;
using MS.SampleDomain.Commands;
using MS.SampleDomain.Contracts;
using MS.SampleDomain.Core.IoC;

namespace MS.SampleDomain
{
    public class SampleService
    {
        private readonly Lazy<IConsumerTypesRegistrar> _consumerTypesRegistrar;
        private Lazy<ICommandPublish> _commandBus;
        private Lazy<IEventPublish> _eventBus;
        private IBusControl _busControl;

        public SampleService(Lazy<IConsumerTypesRegistrar> consumerTypesRegistrar, Lazy<ICommandPublish> commandBus, Lazy<IEventPublish> eventBus)
        {
            if (consumerTypesRegistrar == null) throw new ArgumentNullException("consumerTypesRegistrar");
            if (commandBus == null) throw new ArgumentNullException("commandBus");
            if (eventBus == null) throw new ArgumentNullException("eventBus");
            _consumerTypesRegistrar = consumerTypesRegistrar;
            _commandBus = commandBus;
            _eventBus = eventBus;
        }

        public void Start()
        {

            _busControl = ConfigureBus(
                new EndpointSpecification("sample_command_queue",
                    configurator =>
                        _consumerTypesRegistrar.Value.RegisterConsumers(configurator,
                            type => typeof (ISampleCommandHandler).IsAssignableFrom(type))),
                new EndpointSpecification("sample_event_queue",
                    configurator => _consumerTypesRegistrar.Value.RegisterConsumers(configurator, type => typeof(ISampleEventHandler).IsAssignableFrom(type))));
            
            var commandBus = (PublishBase)_commandBus.Value;
            commandBus.InitializeWith(_busControl);

            var eventBus = (PublishBase)_eventBus.Value;
            eventBus.InitializeWith(_busControl);

            _busControl.Start();

            commandBus.Publish(new NotifyInitializationCompleted());

            var schedulerEndpoint = TaskUtil.Await(() => _busControl.GetSendEndpoint(new Uri("loopback://localhost/quartz")));
            schedulerEndpoint.ScheduleSend(new Uri("loopback://localhost/sample_command_queue"), TimeSpan.FromSeconds(5), new DoPing());
        }

        public void Stop()
        {
            _busControl.Stop();
            _busControl = null;
            _commandBus = null;
            _eventBus = null;
        }

        private static IBusControl ConfigureBus(EndpointSpecification commandEndpoint, EndpointSpecification eventEndpoint)
        {
            var bus = Bus.Factory.CreateUsingInMemory(cfg =>
            {
            //var bus = Bus.Factory.CreateUsingRabbitMq(cfg =>
            //{
            //    cfg.Host(new Uri("rabbitmq://localhost/"), h =>
            //    {
            //        h.Username("guest");
            //        h.Password("guest");
            //    });
                cfg.ReceiveEndpoint(commandEndpoint.QueueName, commandEndpoint.Configure);
                cfg.ReceiveEndpoint(eventEndpoint.QueueName, eventEndpoint.Configure);
                
                cfg.UseInMemoryScheduler();

                //cfg.UseConcurrencyLimit(1);
            });

            //var result = bus.GetProbeResult();
            //Console.WriteLine("###########################################################################");
            //Console.WriteLine(result.ToJsonString());
            //Console.WriteLine("---------------------------------------------------------------------------");

            return bus;
        }
    }
}
