﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MS.SampleDomain.Commands;
using MS.SampleDomain.Contracts;
using MS.SampleDomain.Events;

namespace MS.SampleDomain.Handlers
{
    public class PingEventHandler: ISampleEventHandler, IConsumer<Pinged>
    {
        public async Task Consume(ConsumeContext<Pinged> context)
        {
            await Console.Out.WriteLineAsync(context.Message.Count + ". ping received! " + DateTime.Now);
            await context.ScheduleMessage(new Uri("loopback://localhost/sample_command_queue"), DateTime.Now.AddSeconds(10), new DoPing());
        }
    }
}
