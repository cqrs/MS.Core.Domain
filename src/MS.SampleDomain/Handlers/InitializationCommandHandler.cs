﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MS.Core.Domain;
using MS.SampleDomain.Commands;
using MS.SampleDomain.Contracts;
using MS.SampleDomain.Logic.Aggregates;

namespace MS.SampleDomain.Handlers
{
    public class InitializationCommandHandler : ISampleCommandHandler, IConsumer<NotifyInitializationCompleted>
    {
        private readonly ICommandHandlerContext _handlerContext;

        private static readonly Guid AggregateId = new Guid("60E18DC5-C20E-47B9-8F36-2BBCB04EA0C3");

        public InitializationCommandHandler(ICommandHandlerContext handlerContext)
        {
            _handlerContext = handlerContext;
        }

        public async Task Consume(ConsumeContext<NotifyInitializationCompleted> context)
        {
            InnerProcess aggregate;
            if (_handlerContext.TryGetById(AggregateId, out aggregate))
            {
                await aggregate.NotifyInitialized();
            }
            _handlerContext.Finalize(aggregate, Guid.NewGuid());
        }
    }
}
