﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MS.SampleDomain.Contracts;
using MS.SampleDomain.Events;

namespace MS.SampleDomain.Handlers
{
    public class InitializationEventHandler: ISampleEventHandler, IConsumer<InitializationCompleted>
    {
        public async Task Consume(ConsumeContext<InitializationCompleted> context)
        {
            await Console.Out.WriteLineAsync("Initialization completed!");
        }
    }
}
