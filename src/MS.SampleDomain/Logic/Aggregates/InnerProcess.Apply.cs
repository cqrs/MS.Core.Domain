﻿using MS.Core.Domain;
using MS.SampleDomain.Events;

namespace MS.SampleDomain.Logic.Aggregates
{
    partial class InnerProcess: /*IApplyEvent<InitializationCompleted>,*/ IApplyEvent<Pinged>
    {
        //public void Apply(InitializationCompleted @event, bool isNew)
        //{
        //}

        public void Apply(Pinged @event, bool isNew)
        {
            _pingCount++;
        }
    }
}
