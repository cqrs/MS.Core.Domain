﻿using System.Threading.Tasks;
using MS.Core.Domain.Implementation;
using MS.SampleDomain.Events;

namespace MS.SampleDomain.Logic.Aggregates
{
    internal partial class InnerProcess: AggregateBase
    {
        private long _pingCount;

        public async Task NotifyInitialized()
        {
            await ApplyEvent(new InitializationCompleted());
        }

        public async Task Ping()
        {
            await ApplyEvent(new Pinged { Count = _pingCount + 1 });
        }

        
        protected override void LoadFromSnapshot<T>(T snapshot)
        {
        }
    }
}
