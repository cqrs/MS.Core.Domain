﻿using MS.Core.Domain;

namespace MS.SampleDomain.Events
{
    public class Pinged : IEvent
    {
        public long Count { get; set; }
    }
}
