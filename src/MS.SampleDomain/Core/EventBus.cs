﻿using MS.Core.Domain;
using MS.Core.Domain.Implementation;

namespace MS.SampleDomain.Core
{
    public class EventBus : PublishBase, IEventPublish
    {
    }
}
