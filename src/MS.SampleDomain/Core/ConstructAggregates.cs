﻿using System;
using MS.Core.Domain;
using MS.Core.Domain.Implementation;
using MS.Core.Domain.Persistence;

namespace MS.SampleDomain.Core
{
    public class ConstructAggregates : IConstructAggregates
    {
        private readonly Func<Type, object> _factory;

        public ConstructAggregates(Tuple<Func<Type, object>> factory)
        {
            if (factory == null) throw new ArgumentNullException("factory");
            _factory = factory.Item1;
        }

        public IAggregate Build(Type type, Guid id, IMemento snapshot)
        {
            if (!type.IsSubclassOf(typeof(AggregateBase)))
            {
                throw new InvalidCastException(string.Format("Can't cast type {0} to {1}", type.FullName, typeof(AggregateBase).FullName));
            }
            var aggregate = (AggregateBase)_factory(type);
            aggregate.Id = id;
            if (snapshot != null)
            {
                aggregate.InitializeFromSnapshot(snapshot);
            }
            return aggregate;
        }
    }
}
