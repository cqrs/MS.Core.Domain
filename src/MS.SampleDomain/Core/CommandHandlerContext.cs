﻿using MS.Core.Domain;
using MS.Core.Domain.Implementation;
using MS.Core.Domain.Persistence;

namespace MS.SampleDomain.Core
{
    public interface ICommandHandlerContext : MS.Core.Domain.ICommandHandlerContext
    {
    }

    public class CommandHandlerContext : CommandHandlerContextBase, ICommandHandlerContext
    {
        public CommandHandlerContext(IRepository aggregates, IEventPublish eventBus) : base(aggregates, eventBus)
        {
        }
    }
}
