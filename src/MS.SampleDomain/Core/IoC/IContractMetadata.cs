﻿using System;

namespace MS.SampleDomain.Core.IoC
{
    public interface IContractMetadata
    {
        Type ContractType { get; }
    }
}
