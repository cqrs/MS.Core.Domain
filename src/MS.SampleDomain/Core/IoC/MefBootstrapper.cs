﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Registration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using NEventStore;
using NEventStore.Contrib.Persistence;
using NEventStore.Contrib.Persistence.SqlDialects;
using NEventStore.Conversion;
using NEventStore.Persistence;
using NEventStore.Persistence.InMemory;
using NEventStore.Serialization;

namespace MS.SampleDomain.Core.IoC
{
    public class MefBootstrapper<TRootModel> : Bootstrapper<TRootModel>
    {
        const string ConnectionString = "User=SYSDBA;Password=doesntmatter;Database=neventstore.fdb;DataSource=localhost;Port=3050;Dialect=3;Charset=UTF8;Role=;Connection lifetime=15;Pooling=true;MinPoolSize=0;MaxPoolSize=50;Packet Size=8192;ServerType=1;";

        protected ExportProvider Provider;

        // ReSharper disable EmptyConstructor 
        // ReSharper disable RedundantBaseConstructorCall
        public MefBootstrapper() : base()
        {
        }

        public MefBootstrapper(Assembly executingAssembly) : base(executingAssembly)
        {
        }

        // ReSharper restore RedundantBaseConstructorCall
        // ReSharper restore EmptyConstructor

        protected override void Configure(Assembly executingAssembly)
        {
            var catalog = new AggregateCatalog();

            var funcCatalog = new FuncCatalog();

            catalog.Catalogs.Add(new RegistrationBuilder().Registrate().GetCatalog(funcCatalog));

            var container = new CompositionContainer(catalog, CompositionOptions.DisableSilentRejection | CompositionOptions.IsThreadSafe);

            var batch = new CompositionBatch();
            batch.AddExportedValue<ExportProvider>(container);
            BatchConfig(container, batch);
            Wireup(batch);

            container.Compose(batch);
            container.SatisfyImportsOnce(this);
            Provider = container;
            //ServiceLocator.SetLocatorProvider(() => new MefServiceLocator(container)); // provide common service locator, if needed
        }

        protected virtual void BatchConfig(CompositionContainer container, CompositionBatch batch)
        {
            Func<Type, object> objectFactory =
                type => (container.GetExportedValue<object>(AttributedModelServices.GetContractName(type)));
            Func<Type, IEnumerable<object>> objectListFactory =
                type => (container.GetExportedValues<object>(AttributedModelServices.GetContractName(type)));
            batch.AddExportedValue(new Tuple<Func<Type, object>>(objectFactory)); //Tuple envelope is a stupid workaround
            batch.AddExportedValue(new Tuple<Func<Type, IEnumerable<object>>>(objectListFactory));
        }

        protected override T GetExportedValue<T>()
        {
            return Provider.GetExportedValue<T>();
        }

        private void Wireup(CompositionBatch batch)
        {
            var initdb = false;
            if (!File.Exists("neventstore.fdb"))
            {
                FbConnection.ClearAllPools();
                FbConnection.CreateDatabase(ConnectionString, false);
                initdb = true;
            }
            
            var storeEvents = NEventStore.Wireup
                .Init()
                .LogToOutputWindow()
                .UsingFirebirdConnectionString("NEventStore", ConnectionString, "FirebirdSql.Data.FirebirdClient", initdb)
                .WithDialect(new FirebirdSqlDialect())
                .UsingJsonSerialization()
                //.Compress()
                .Build();
            
            batch.AddExportedValue(storeEvents);
        }

        private static IStoreEvents BuildEventStore(NanoContainer context)
        {
            var concurrency = new OptimisticPipelineHook();
            var upconverter = context.Resolve<EventUpconverterPipelineHook>();

            var hooks = context.Resolve<ICollection<IPipelineHook>>() ?? new IPipelineHook[0];
            hooks = new IPipelineHook[] { concurrency, upconverter }
                .Concat(hooks)
                .Where(x => x != null)
                .ToArray();

            return new OptimisticEventStore(context.Resolve<IPersistStreams>(), hooks);
        }
    }
}