using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;

namespace MS.SampleDomain.Core.IoC.Delegates
{
    internal class Func0PartDefinition<TOut> : DelegatePartDefinitionBase
    {
        // ReSharper disable once StaticFieldInGenericType
        private static readonly ContractBasedImportDefinition ExportProviderImportDefinition;
        private readonly List<ExportDefinition> _exportDefinitions = new List<ExportDefinition>();
        private readonly List<ImportDefinition> _importDefinitions = new List<ImportDefinition>();

        static Func0PartDefinition()
        {
            var importContractName = typeof (ExportProvider).ToString();
            ExportProviderImportDefinition = new ContractBasedImportDefinition(
                importContractName,
                AttributedModelServices.GetTypeIdentity(typeof (ExportProvider)),
                null,
                ImportCardinality.ZeroOrOne,
                false,
                false,
                CreationPolicy.Any);
        }

        public Func0PartDefinition(Func<ExportProvider, Func<TOut>> factory)
        {
            _exportDefinitions.Add(
                new Func0ExportDefinition<TOut>(factory));
            _importDefinitions.Add(ExportProviderImportDefinition);
        }

        public override IEnumerable<ExportDefinition> ExportDefinitions
        {
            get { return _exportDefinitions; }
        }

        public override IEnumerable<ImportDefinition> ImportDefinitions
        {
            get { return _importDefinitions; }
        }

        public override ComposablePart CreatePart()
        {
            return new Func0Part<TOut>(this);
        }
    }
}