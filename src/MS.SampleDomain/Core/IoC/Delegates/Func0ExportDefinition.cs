﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;

namespace MS.SampleDomain.Core.IoC.Delegates
{
    internal class Func0ExportDefinition<TOut> : ExportDefinition
    {
        public Func0ExportDefinition(
            Func<ExportProvider, Func<TOut>> factory) :
                base(AttributedModelServices.GetTypeIdentity(typeof(Func<Func<TOut>>)),
                    new Dictionary<string, object>
                    {
                        {"ExportTypeIdentity", AttributedModelServices.GetTypeIdentity(typeof(Func<Func<TOut>>))}
                    })
        {
            Factory = factory;
        }

        public Func<ExportProvider, Func<TOut>> Factory { get; private set; }
    }
}