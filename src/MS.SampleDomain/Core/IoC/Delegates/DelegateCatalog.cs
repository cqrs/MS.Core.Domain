﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq;

namespace MS.SampleDomain.Core.IoC.Delegates
{
    public class DelegateCatalog : ComposablePartCatalog, INotifyComposablePartCatalogChanged
    {
        private readonly List<ComposablePartDefinition> _parts = new List<ComposablePartDefinition>();

        public override IQueryable<ComposablePartDefinition> Parts
        {
            get { return _parts.AsQueryable(); }
        }

        public event EventHandler<ComposablePartCatalogChangeEventArgs> Changed = delegate { };

        public event EventHandler<ComposablePartCatalogChangeEventArgs> Changing = delegate { };

        public DelegatePartDefinitionBase AddFuncPart<TOut>(Func<ExportProvider, Func<TOut>> factory)
        {
            var definition = new Func0PartDefinition<TOut>(factory);
            _parts.Add(definition);
            Changed(this,
                new ComposablePartCatalogChangeEventArgs(new[] {definition}, new Func0PartDefinition<TOut>[] {}, null));
            return definition;
        }

        public DelegatePartDefinitionBase AddFuncPart<TIn, TOut>(Func<ExportProvider, Func<TIn, TOut>> factory)
        {
            var definition = new Func1PartDefinition<TIn, TOut>(factory);
            _parts.Add(definition);
            Changed(this,
                new ComposablePartCatalogChangeEventArgs(new[] { definition }, new Func1PartDefinition<TIn, TOut>[] { }, null));
            return definition;
        }

        public void AddParts(params DelegatePartDefinitionBase[] parts)
        {
            _parts.AddRange(parts);
            Changed(this, new ComposablePartCatalogChangeEventArgs(parts, new DelegatePartDefinitionBase[] {}, null));
        }

        public void RemoveParts(params DelegatePartDefinitionBase[] parts)
        {
            _parts.RemoveAll(parts.Contains);
            Changed(this, new ComposablePartCatalogChangeEventArgs(new DelegatePartDefinitionBase[] {}, parts, null));
        }
    }
}