﻿using System.ComponentModel.Composition.Primitives;

namespace MS.SampleDomain.Core.IoC.Delegates
{
    public abstract class DelegatePartDefinitionBase : ComposablePartDefinition
    {
    }
}