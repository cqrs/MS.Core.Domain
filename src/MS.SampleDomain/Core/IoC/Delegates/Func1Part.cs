﻿using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq;

namespace MS.SampleDomain.Core.IoC.Delegates
{
    internal class Func1Part<TIn, TOut> : ComposablePart
    {
        private readonly Func1PartDefinition<TIn, TOut> _definition;
        private ExportProvider _provider;

        public Func1Part(Func1PartDefinition<TIn, TOut> definition)
        {
            _definition = definition;
        }

        public override IEnumerable<ExportDefinition> ExportDefinitions
        {
            get { return _definition.ExportDefinitions; }
        }

        public override IEnumerable<ImportDefinition> ImportDefinitions
        {
            get { return _definition.ImportDefinitions; }
        }

        public override object GetExportedValue(ExportDefinition definition)
        {
            var delegateExportDefinition = definition as Func1ExportDefinition<TIn, TOut>;
            // ReSharper disable once PossibleNullReferenceException
            return delegateExportDefinition.Factory(_provider);
        }

        public override void SetImport(ImportDefinition definition, IEnumerable<Export> exports)
        {
            _provider = exports.First().Value as ExportProvider;
        }
    }
}