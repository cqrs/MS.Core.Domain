﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;

namespace MS.SampleDomain.Core.IoC.Delegates
{
    internal class Func1ExportDefinition<TIn, TOut> : ExportDefinition
    {
        public Func1ExportDefinition(    
            Func<ExportProvider, Func<TIn, TOut>> factory) :
            base(AttributedModelServices.GetTypeIdentity(typeof(Func<Func<TIn, TOut>>)),
                    new Dictionary<string, object>
                    {
                        {"ExportTypeIdentity", AttributedModelServices.GetTypeIdentity(typeof(Func<Func<TIn, TOut>>))}
                    })
        {
            Factory = factory;
        }

        public Func<ExportProvider, Func<TIn, TOut>> Factory { get; private set; }
    }
}