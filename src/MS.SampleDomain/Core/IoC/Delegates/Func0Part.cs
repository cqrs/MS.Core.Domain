﻿using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq;

namespace MS.SampleDomain.Core.IoC.Delegates
{
    internal class Func0Part<TContract> : ComposablePart
    {
        private readonly Func0PartDefinition<TContract> _definition;
        private ExportProvider _provider;

        public Func0Part(Func0PartDefinition<TContract> definition)
        {
            _definition = definition;
        }

        public override IEnumerable<ExportDefinition> ExportDefinitions
        {
            get { return _definition.ExportDefinitions; }
        }

        public override IEnumerable<ImportDefinition> ImportDefinitions
        {
            get { return _definition.ImportDefinitions; }
        }

        public override object GetExportedValue(ExportDefinition definition)
        {
            var delegateExportDefinition = definition as Func0ExportDefinition<TContract>;
            // ReSharper disable once PossibleNullReferenceException
            return delegateExportDefinition.Factory(_provider);
        }

        public override void SetImport(ImportDefinition definition, IEnumerable<Export> exports)
        {
            _provider = exports.First().Value as ExportProvider;
        }
    }
}