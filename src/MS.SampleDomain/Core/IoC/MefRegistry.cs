﻿using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.ComponentModel.Composition.Registration;
using System.Linq;
using MassTransit;
using MS.Core.Domain;
using MS.Core.Domain.Implementation;
using MS.Core.Domain.Persistence.EventStore;
using MS.SampleDomain.Contracts;
using NEventStore;

namespace MS.SampleDomain.Core.IoC
{
    public static class MefRegistry
    {
        public static RegistrationBuilder Registrate(this RegistrationBuilder registration)
        {
            RegisterEventStore(registration);
            RegisterHandlers(registration);
            RegisterHandlerContexts(registration);
            RegisterPublishers(registration);
            RegisterAggregates(registration);
            RegisterService(registration);

            return registration;
        }


        public static ComposablePartCatalog GetCatalog(this RegistrationBuilder registration, params ComposablePartCatalog[] catalogs)
        {
            var catalogList = catalogs.ToList();
            
            catalogList.Add(new AssemblyCatalog(typeof(IStoreEvents).Assembly, registration));
            catalogList.Add(new AssemblyCatalog(typeof(IStoreEvents).Assembly, registration));
            catalogList.Add(new AssemblyCatalog(typeof(Bus).Assembly, registration));
            catalogList.Add(new AssemblyCatalog(typeof(MS.Core.Domain.ICommandHandlerContext).Assembly, registration));
            catalogList.Add(new AssemblyCatalog(typeof(PublishBase).Assembly, registration));
            catalogList.Add(new AssemblyCatalog(typeof(EventStoreRepository).Assembly, registration));
            catalogList.Add(new AssemblyCatalog(typeof(SampleService).Assembly, registration));
            return new AggregateCatalog(catalogList.ToArray());
        }

        private static void RegisterSpecialFactories(FuncCatalog funcCatalog)
        {
            //funcCatalog.AddPart<Func<Type, object>>(
            //    provider => new Func<Type, object>(
            //        type => provider.GetExportedValue<object>(AttributedModelServices.GetContractName(type))));
            //funcCatalog.AddPart<Func<Type, IEnumerable<object>>>(
            //    provider => new Func<Type, IEnumerable<object>>(
            //        type => provider.GetExportedValues<object>(AttributedModelServices.GetContractName(type))));
        }

        private static void RegisterEventStore(RegistrationBuilder registration)
        {
            registration.ForType<ConstructAggregates>()
                .SetCreationPolicy(CreationPolicy.NonShared)
                .Export()
                .ExportInterfaces();

            registration.ForType<EventStoreRepository>()
                .SetCreationPolicy(CreationPolicy.NonShared)
                .Export()
                .ExportInterfaces();

            registration.ForType<ConflictDetector>()
                .SetCreationPolicy(CreationPolicy.NonShared)
                .Export()
                .ExportInterfaces();
        }

        private static void RegisterPublishers(RegistrationBuilder registration)
        {
            registration.ForTypesDerivedFrom<ICommandPublish>()
                .SetCreationPolicy(CreationPolicy.Shared)
                .Export()
                .ExportInterfaces();
            registration.ForTypesDerivedFrom<IEventPublish>()
                .SetCreationPolicy(CreationPolicy.Shared)
                .Export()
                .ExportInterfaces();
        }

        private static void RegisterHandlers(RegistrationBuilder registration)
        {
            registration.ForType<ConsumerTypesRegistrar>()
                .SetCreationPolicy(CreationPolicy.Shared)
                .ExportInterfaces();

            registration.ForTypesDerivedFrom<ISampleCommandHandler>()
                .SetCreationPolicy(CreationPolicy.NonShared)
                .Export()
                .Export<ISampleCommandHandler>()
                .Export<IConsumer>(builder => builder.AddMetadata("ContractType", type => type));

            registration.ForTypesDerivedFrom<ISampleEventHandler>()
                .SetCreationPolicy(CreationPolicy.NonShared)
                .Export()
                .Export<ISampleEventHandler>()
                .Export<IConsumer>(builder => builder.AddMetadata("ContractType", type => type));
        }

        private static void RegisterHandlerContexts(RegistrationBuilder registration)
        {
            registration.ForTypesDerivedFrom<MS.Core.Domain.ICommandHandlerContext>()
                .SetCreationPolicy(CreationPolicy.NonShared)
                .Export()
                .ExportInterfaces();
        }

        private static void RegisterAggregates(RegistrationBuilder registration)
        {
            registration.ForTypesDerivedFrom<IAggregate>()
                .SetCreationPolicy(CreationPolicy.NonShared)
                .Export()
                .ExportInterfaces();
        }
        
        private static void RegisterService(RegistrationBuilder registration)
        {
            registration.ForType<SampleService>()
                .SetCreationPolicy(CreationPolicy.Shared)
                .Export();
        }
    }
}