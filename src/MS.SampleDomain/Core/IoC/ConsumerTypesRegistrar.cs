﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using MassTransit;

namespace MS.SampleDomain.Core.IoC
{
    public class ConsumerTypesRegistrar : IConsumerTypesRegistrar
    {
        private readonly ExportProvider _exportProvider;
        private readonly IEnumerable<Lazy<IConsumer, IContractMetadata>> _exports;

        public ConsumerTypesRegistrar(ExportProvider provider)
        {
            if (provider == null) throw new ArgumentNullException("provider");
            _exportProvider = provider;
            _exports = _exportProvider.GetExports<IConsumer, IContractMetadata>();
        }

        public void RegisterConsumers(IReceiveEndpointConfigurator configurator, Predicate<Type> filter)
        {
            foreach (var type in _exports
                .Select(contractType => contractType.Metadata.ContractType)
                .Where(type => filter(type)))
            {
                configurator.Consumer(type, t => _exportProvider.GetExportedValue<object>(AttributedModelServices.GetContractName(t)));
            }
        }
    }
}
