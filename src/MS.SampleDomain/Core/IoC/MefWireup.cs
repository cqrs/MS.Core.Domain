﻿using NEventStore;

namespace MS.SampleDomain.Core.IoC
{
    public class MefWireup: Wireup
    {
        public MefWireup(NanoContainer container) : base(container)
        {
        }

        protected MefWireup(Wireup inner) : base(inner)
        {
        }
    }
}
