using System;
using MassTransit;

namespace MS.SampleDomain.Core.IoC
{
    public interface IConsumerTypesRegistrar
    {
        void RegisterConsumers(IReceiveEndpointConfigurator configurator, Predicate<Type> filter);
    }
}