﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq;
using NEventStore;

namespace MS.SampleDomain.Core.IoC
{
    public class MefNanoContainer : NanoContainer
    {
        private readonly NanoExportProvider _exportProvider;

        public ExportProvider ExportProvider
        {
            get { return _exportProvider; }
        }

        public MefNanoContainer()
        {
            _exportProvider = new NanoExportProvider(this);
        }

        public override ContainerRegistration Register<TService>(Func<NanoContainer, TService> resolve)
        {
            _exportProvider.Register<TService>();
            return base.Register(resolve);
        }

        public override ContainerRegistration Register<TService>(TService instance)
        {
            _exportProvider.Register<TService>();
            return base.Register(instance);
        }

        private class NanoExportProvider : ExportProvider
        {
            private readonly NanoContainer _container;
            private readonly Dictionary<string, Export> _exports;
            
            public NanoExportProvider(NanoContainer container)
            {
                _container = container;
                _exports = new Dictionary<string, Export>();
            }

            public void Register<TService>()
            {
                var metadata = new Dictionary<string, object>();
                var contractName = AttributedModelServices.GetContractName(typeof(TService));
                metadata.Add(CompositionConstants.ExportTypeIdentityMetadataName, contractName);
                var exportDefinition = new ExportDefinition(contractName, metadata);
                var export = new Export(exportDefinition, () => _container.Resolve<TService>());
                _exports[contractName] = export;
            }

            protected override IEnumerable<Export> GetExportsCore(ImportDefinition definition, AtomicComposition atomicComposition)
            {
                return _exports.Values.Where(x => definition.IsConstraintSatisfiedBy(x.Definition));
            }
        }
    }
}
