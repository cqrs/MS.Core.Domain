﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq;

namespace MS.SampleDomain.Core.IoC
{
    public abstract class FuncPartDefinitionBase : ComposablePartDefinition
    {
    }

    public class FuncCatalog : ComposablePartCatalog, INotifyComposablePartCatalogChanged
    {
        private readonly List<ComposablePartDefinition> _parts = new List<ComposablePartDefinition>();

        public override IQueryable<ComposablePartDefinition> Parts
        {
            get { return _parts.AsQueryable(); }
        }

        public event EventHandler<ComposablePartCatalogChangeEventArgs> Changed = delegate { };

        public event EventHandler<ComposablePartCatalogChangeEventArgs> Changing = delegate { };

        public FuncPartDefinitionBase AddPart<T>(Func<ExportProvider, object> factory)
        {
            var definition = new FuncPartDefinition<T>(factory);
            _parts.Add(definition);
            Changed(this,
                new ComposablePartCatalogChangeEventArgs(new[] {definition}, new FuncPartDefinition<T>[] {}, null));
            return definition;
        }

        public void AddParts(params FuncPartDefinitionBase[] parts)
        {
            _parts.AddRange(parts);
            Changed(this, new ComposablePartCatalogChangeEventArgs(parts, new FuncPartDefinitionBase[] {}, null));
        }

        public void RemoveParts(params FuncPartDefinitionBase[] parts)
        {
            _parts.RemoveAll(parts.Contains);
            Changed(this, new ComposablePartCatalogChangeEventArgs(new FuncPartDefinitionBase[] {}, parts, null));
        }
    }

    internal class FuncPartDefinition<TContract> : FuncPartDefinitionBase
    {
        // ReSharper disable once StaticFieldInGenericType
        private static readonly ContractBasedImportDefinition ExportProviderImportDefinition;
        private readonly List<ExportDefinition> _exportDefinitions = new List<ExportDefinition>();
        private readonly List<ImportDefinition> _importDefinitions = new List<ImportDefinition>();

        static FuncPartDefinition()
        {
            var importContractName = typeof (ExportProvider).ToString();
            ExportProviderImportDefinition = new ContractBasedImportDefinition(
                importContractName,
                AttributedModelServices.GetTypeIdentity(typeof (ExportProvider)),
                null,
                ImportCardinality.ZeroOrOne,
                false,
                false,
                CreationPolicy.Any);
        }

        public FuncPartDefinition(Func<ExportProvider, object> factory)
        {
            _exportDefinitions.Add(
                new FuncExportDefinition(typeof (TContract), factory));
            _importDefinitions.Add(ExportProviderImportDefinition);
        }

        public FuncPartDefinition()
            : this(ep => (TContract) Activator.CreateInstance(typeof (TContract)))
        {
        }

        public override IEnumerable<ExportDefinition> ExportDefinitions
        {
            get { return _exportDefinitions; }
        }

        public override IEnumerable<ImportDefinition> ImportDefinitions
        {
            get { return _importDefinitions; }
        }

        public override ComposablePart CreatePart()
        {
            return new FuncPart<TContract>(this);
        }
    }

    internal class FuncPart<TContract> : ComposablePart
    {
        private readonly FuncPartDefinition<TContract> _definition;
        private ExportProvider _provider;

        public FuncPart(FuncPartDefinition<TContract> definition)
        {
            _definition = definition;
        }

        public override IEnumerable<ExportDefinition> ExportDefinitions
        {
            get { return _definition.ExportDefinitions; }
        }

        public override IEnumerable<ImportDefinition> ImportDefinitions
        {
            get { return _definition.ImportDefinitions; }
        }

        public override object GetExportedValue(ExportDefinition definition)
        {
            var funcExportDefinition = definition as FuncExportDefinition;
            // ReSharper disable once PossibleNullReferenceException
            return funcExportDefinition.Factory(_provider);
        }

        public override void SetImport(ImportDefinition definition, IEnumerable<Export> exports)
        {
            _provider = exports.First().Value as ExportProvider;
        }
    }

    internal class FuncExportDefinition : ExportDefinition
    {
        public FuncExportDefinition(Type exportedType,
            Func<ExportProvider, object> factory) :
                base(AttributedModelServices.GetTypeIdentity(exportedType),
                    new Dictionary<string, object>
                    {
                        {"ExportTypeIdentity", AttributedModelServices.GetTypeIdentity(exportedType)}
                    })
        {
            Factory = factory;
        }

        public Func<ExportProvider, object> Factory { get; private set; }
    }
}