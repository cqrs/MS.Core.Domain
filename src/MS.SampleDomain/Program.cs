﻿using System;
using MS.SampleDomain.Core.IoC;

namespace MS.SampleDomain
{
    class Program
    {
        static void Main(string[] args)
        {
            var bootstrapper = new MefBootstrapper<SampleService>();
            var service = bootstrapper.Root;
            service.Start();
            Console.ReadLine();
            service.Stop();
        }
    }
}
