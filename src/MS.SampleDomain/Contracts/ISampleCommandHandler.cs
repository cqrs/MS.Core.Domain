﻿using MassTransit;

namespace MS.SampleDomain.Contracts
{
    public interface ISampleCommandHandler : IConsumer
    {
    }
}
