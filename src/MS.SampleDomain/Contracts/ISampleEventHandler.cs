﻿using MassTransit;

namespace MS.SampleDomain.Contracts
{
    public interface ISampleEventHandler : IConsumer
    {
    }
}
