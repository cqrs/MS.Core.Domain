﻿using System;
using MassTransit;

namespace MS.Core.Domain.Implementation
{
    public class EndpointSpecification
    {
        private readonly string _queueName;
        private readonly Action<IReceiveEndpointConfigurator> _configure;

        public EndpointSpecification(string queueName, Action<IReceiveEndpointConfigurator> configure)
        {
            if (configure == null) throw new ArgumentNullException("configure");
            if (string.IsNullOrWhiteSpace(queueName)) throw new ArgumentNullException("queueName");
            _queueName = queueName;
            _configure = configure;
        }

        /// <summary>
        /// The default queue name for the endpoint
        /// </summary>
        public string QueueName
        {
            get { return _queueName; }
        }

        /// <summary>
        /// Configures the endpoint, with consumers, handlers, sagas, etc.
        /// </summary>
        public void Configure(IReceiveEndpointConfigurator configurator)
        {
            _configure(configurator);
        }

    }
}
