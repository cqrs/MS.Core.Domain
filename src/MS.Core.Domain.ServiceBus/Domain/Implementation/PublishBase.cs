﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MassTransit;

namespace MS.Core.Domain.Implementation
{
    public abstract class PublishBase : IPublish
    {
        private IBusControl _bus;
        private bool _initialized;
        private bool _disposed;

        ~PublishBase()
        {
            Dispose(false);
        }

        public void InitializeWith(IBusControl bus)
        {
            if (_initialized) return;
            if (bus == null) throw new ArgumentNullException("bus");
            _bus = bus;
            _initialized = true;
        }

        private void CheckInitialized()
        {
            if (!_initialized) throw new InvalidOperationException("Instance has to be initialized before!");
        }

        public virtual Task Publish<T>(T message, CancellationToken cancellationToken = new CancellationToken()) where T : class
        {
            CheckInitialized();
            return _bus.Publish(message, cancellationToken);
        }

        public virtual Task Publish(object message, CancellationToken cancellationToken = new CancellationToken())
        {
            CheckInitialized();
            return _bus.Publish(message, cancellationToken);
        }

        public virtual Task Publish(object message, Type messageType, CancellationToken cancellationToken = new CancellationToken())
        {
            CheckInitialized();
            return _bus.Publish(message, messageType, cancellationToken);
        }

        public virtual Task Publish<T>(object values, CancellationToken cancellationToken = new CancellationToken()) where T : class
        {
            CheckInitialized();
            return _bus.Publish(values, cancellationToken);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            _bus = null;

            _disposed = true;
            _initialized = false;
        }
    }
}
