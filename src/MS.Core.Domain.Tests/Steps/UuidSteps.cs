﻿using System;
using FluentAssertions;
using TechTalk.SpecFlow;

namespace MS.Core.Domain.Tests.Steps
{
    [Binding]
    public class UuidSteps
    {
        [Given(@"a new Guid")]
        public void GivenANewGuid()
        {
            var guid = Guid.NewGuid();
            ScenarioContext.Current.Set(guid);
        }
        
        [Given(@"a first Uuid initialized from this new Guid")]
        public void GivenAFirstUuidInitializedFromThisNewGuid()
        {
            var guid = ScenarioContext.Current.Get<Guid>();
            var uid = Uuid.Empty();
            uid.AsGuid = guid;
            ScenarioContext.Current.Set(uid, "uid1");
        }
        
        [Given(@"a second Uuid initialized from this new Guid")]
        public void GivenASecondUuidInitializedFromThisNewGuid()
        {
            var guid = ScenarioContext.Current.Get<Guid>();
            var uid = Uuid.Empty();
            uid.AsGuid = guid;
            ScenarioContext.Current.Set(uid, "uid2");
        }
        
        [When(@"both Uuids will be compared by method Equals")]
        public void WhenBothUuidsWillBeComparedByMethodEquals()
        {
            var uid1 = ScenarioContext.Current.Get<Uuid>("uid1");
            var uid2 = ScenarioContext.Current.Get<Uuid>("uid2");
            var result = uid1.Equals(uid2);
            ScenarioContext.Current.Set(result);
        }
        
        [Then(@"the equation result should be true")]
        public void ThenTheEquationResultShouldBeTrue()
        {
            var result = ScenarioContext.Current.Get<bool>();
            result.Should().BeTrue();
        }
    }
}
