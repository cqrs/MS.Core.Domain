﻿Feature: Uuid

Scenario: Two Uuids created from same Guid are equal
	Given a new Guid
	And a first Uuid initialized from this new Guid
	And a second Uuid initialized from this new Guid
	When both Uuids will be compared by method Equals
	Then the equation result should be true