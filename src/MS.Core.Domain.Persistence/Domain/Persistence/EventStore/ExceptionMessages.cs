﻿using System;

namespace MS.Core.Domain.Persistence.EventStore
{
    public static class ExceptionMessages
    {
        public static Func<string> ConflictingCommand = () => "The command issued conflicted with another command that was sent by another user, actor, or process in the system.  The change could not be automatically merged.  Please review the data that has changed and try your change again.";
        public static Func<string> NoWork = () => "There were no uncommitted changes to persist.  When attempting to save an aggregate there must be at least one uncommitted event to persist.";
        public static Func<string> NullArgument = () => "The argument cannot be null.";
    }
}
